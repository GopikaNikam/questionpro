package questionpro.dao;

import java.util.List;

import questionpro.model.Story;

public interface StoryDAO {
	
	public boolean saveStory(Story story);
	
	public List<Story> fetchStories();
	
	public List<Story> fetchAllStories();
	
	public List<Story> fetchCommentStories();
}
