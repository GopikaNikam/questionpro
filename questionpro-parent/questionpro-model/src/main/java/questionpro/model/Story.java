package questionpro.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Story {

	@JsonProperty("id")
	private int id;
	
	@JsonProperty("type")
	private String type;
	
	@JsonProperty("by")
	private String by;
	
	@JsonProperty("text")
	private String text ;
	
	@JsonProperty("parent")
	private String parent;
	
	@JsonProperty("poll")
	private int poll;
	
	@JsonProperty("title")
	private String title;

	@JsonProperty("url")
	private String url;
	
	@JsonProperty("score")
	private int score;
	
	@JsonProperty("time")
	private Date time;
	
	@JsonProperty("descendants")
	private int descendants;
	
	@JsonProperty("kids")
	private List<Integer> kids;
	
	@JsonProperty("parts")
	private List<Integer> parts;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getBy() {
		return by;
	}

	public void setBy(String by) {
		this.by = by;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public int getPoll() {
		return poll;
	}

	public void setPoll(int poll) {
		this.poll = poll;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public int getDescendants() {
		return descendants;
	}

	public void setDescendants(int descendants) {
		this.descendants = descendants;
	}

	public List<Integer> getKids() {
		return kids;
	}

	public void setKids(List<Integer> kids) {
		this.kids = kids;
	}

	public List<Integer> getParts() {
		return parts;
	}

	public void setParts(List<Integer> parts) {
		this.parts = parts;
	}

	@Override
	public String toString() {
		return "Story [id=" + id + ", type=" + type + ", by=" + by + ", text=" + text + ", parent=" + parent + ", poll="
				+ poll + ", title=" + title + ", url=" + url + ", score=" + score + ", time=" + time + ", descendants="
				+ descendants + ", kids=" + kids + ", parts=" + parts + "]";
	}
}
