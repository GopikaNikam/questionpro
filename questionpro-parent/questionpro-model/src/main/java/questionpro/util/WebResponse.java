package questionpro.util;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(Include.NON_NULL)
public class WebResponse {
	
	@JsonProperty("header")
	private WebHeader header;
	
	@JsonProperty("body")
	private Object body;
	
	@JsonProperty("errors")	
	private List<ErrorInfo> errors;

	/**
	 * @return the header
	 */
	public WebHeader getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(WebHeader header) {
		this.header = header;
	}

	/**
	 * @return the body
	 */
	public Object getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(Object body) {
		this.body = body;
	}

	/**
	 * @return the errors
	 */
	public List<ErrorInfo> getErrors() {
		return errors;
	}

	/**
	 * @param errors the errors to set
	 */
	public void setErrors(List<ErrorInfo> errors) {
		this.errors = errors;
	}	
}
