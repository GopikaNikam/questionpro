package questionpro.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AbstractJDBCDAO {

	private final static Logger logger = LoggerFactory.getLogger(AbstractJDBCDAO.class);
	
	protected SQLQueryMap queryMap;
	
	private Map<DatabaseEnum, SQLQueryMap> otherDbQueryMaps;

	public AbstractJDBCDAO() {
	}

	public void initialize() {
		
		if(otherDbQueryMaps != null) {
			try {
				/** get the currently connected database vendor */
				DatabaseEnum db = getDatabaseType();
				
				if(db != null) {
					SQLQueryMap map = otherDbQueryMaps.get(db);
					if(map != null) {
						/** this will overwrite queries having same queryId as per the currently connected database. **/
						queryMap.merge(map);						
					}
				}
			}catch(SQLException sqle) {
				throw new RuntimeException(sqle);
			}
		}
	}
	
	protected abstract DataSource getDataSource();

	/**
	 * @return the queryMap
	 */
	public SQLQueryMap getQueryMap() {
		return queryMap;
	}

	/**
	 * @param queryMap the queryMap to set
	 */
	public void setQueryMap(SQLQueryMap queryMap) {
		this.queryMap = queryMap;
	}

	/**
	 * @return the otherDbQueryMaps
	 */
	public Map<DatabaseEnum, SQLQueryMap> getOtherDbQueryMaps() {
		return otherDbQueryMaps;
	}

	/**
	 * @param otherDbQueryMaps the otherDbQueryMaps to set
	 */
	public void setOtherDbQueryMaps(Map<DatabaseEnum, SQLQueryMap> otherDbQueryMaps) {
		this.otherDbQueryMaps = otherDbQueryMaps;
	}

	/**
	 * Get the database product, e.g. mysql, oracle.
	 * 
	 * @return
	 * @throws SQLException
	 */
	protected DatabaseEnum getDatabaseType() throws SQLException {
		Connection con = getDataSource().getConnection();
		String product = con.getMetaData().getDatabaseProductName();
		close(con, null);
		
		logger.debug("Identiied database type is: {}", product);
		
		if("MySQL".equalsIgnoreCase(product)) {
			return DatabaseEnum.MYSQL;
		}
		
		if("Oracle".equalsIgnoreCase(product)) {
			return DatabaseEnum.ORACLE;
		}
		
		return null;
	}
	
	protected SQLQuery getQuery(String queryID) {
		return queryMap.getQuery(queryID);
	}
	
	/**
	 * Creates a connection from the datasource using getDatasource() and 
	 * prepares and returns a PreparedStatement object.
	 * 
	 * @param query
	 * @param object
	 * @return
	 * @throws SQLException
	 */
	protected PreparedStatement getStatement(String queryID, SQLQueryObject object) throws SQLException {
		SQLQuery query = queryMap.getQuery(queryID);
		return createStatement(query, object);
	}
	
	/**
	 * Prepares a statement and returns using the connection passed.
	 * This useful when selecting/updating multiple tables in a single transaction. 
	 *
	 * 
	 * @param queryID
	 * @param object
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	protected PreparedStatement getStatement(String queryID, SQLQueryObject object, Connection con) throws SQLException {
		SQLQuery query = queryMap.getQuery(queryID);
		return createStatement(query, object, con);
	}
	
	/**
	 * Creates a connection from the datasource using getDatasource() and 
	 * prepares and returns a PreparedStatement object.
	 * 
	 * @param query
	 * @param object
	 * @return
	 * @throws SQLException
	 */
	protected PreparedStatement createStatement(SQLQuery query, SQLQueryObject object) throws SQLException {
		
		DataSource ds = getDataSource();
		Connection con = ds.getConnection();
		
		return createStatement(query, object, con);
	}
	
	/**
	 * Prepares a statement using the connection passed. This is useful when running
	 * multiple table select/update within the same transaction. 
	 * 
	 * @param query
	 * @param object
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	protected PreparedStatement createStatement(SQLQuery query, SQLQueryObject object, Connection con) throws SQLException {
		
		PreparedStatement statement = con.prepareStatement(query.getQuery());
		
		if(object != null) {
			object.setValues(statement);
		}
		
		return statement;		
	}
	
	/**
	 * Close the resultset, the statement and the underlying connection.
	 * If datasource is used, then connection should be returned back to pool
	 * as per the underlying jdbc driver implementation.
	 * 
	 * @param rs
	 * @param statement
	 */
	protected void close(ResultSet rs, PreparedStatement statement) {
		close(null, rs, statement);
	}
	
	/**
	 * Close the the statement and the underlying connection.
	 * If datasource is used, then connection should be returned back to pool
	 * as per the underlying jdbc driver implementation.
	 * 
	 * @param rs
	 * @param statement
	 */
	protected void close(PreparedStatement statement) {
		close(null, null, statement);
	}
	
	/**
	 * Close the statement only but not the underlying connection.
	 * This is useful when connection is shared amongst multiple methods
	 * and you dont want to close connection in the nested method call,
	 * but want to close statement only.
	 * 
	 * @param statement
	 */
	protected void closeStatement(PreparedStatement statement) {
		close(null, null, statement, false);
	}
	
	/**
	 * Close result set and statement, but not the underlying connection.
	 * 
	 * @param rs
	 * @param statement
	 */
	protected void closeResultSetAndStatement(ResultSet rs, PreparedStatement statement) {
		close(null, rs, statement, false);
	}

	/**
	 * Close the connection and statement.
	 * 
	 * @param con
	 * @param statement
	 */
	protected void close(Connection con, PreparedStatement statement) {
		close(con, null, statement);
	}

	/**
	 * Close Resultset, Statement and Connection in that order.
	 * 
	 * if con is null and statement is not closed, then connection is retrieved from statement and closed.
	 * 
	 * @param con
	 * @param rs
	 * @param statement
	 */
	protected void close(Connection con, ResultSet rs, PreparedStatement statement) {
		close(con, rs, statement, true);
	}
	
	private void close(Connection con, ResultSet rs, PreparedStatement statement, boolean closeConnection) {
		if(rs != null) {
			try {
				 rs.close();
			} catch (SQLException e) {
				logger.warn("Error closing result set!", e);
			}			
		}

		if(statement != null) {
			try {
				if(!statement.isClosed()) {
					if(con == null && closeConnection) {
						con = statement.getConnection();
					}
					statement.close();					
				}
			} catch (SQLException e) {
				logger.warn("Error closing statement!", e);
			}
		}
		
		if(con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				logger.warn("Error closing connection!", e);
			}
		}
	}
}