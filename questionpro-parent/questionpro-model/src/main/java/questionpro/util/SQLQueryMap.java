package questionpro.util;

import java.util.HashMap;

public class SQLQueryMap {

	private HashMap<String, SQLQuery> queryMap;

	public HashMap<String, SQLQuery> getQueryMap() {
		return queryMap;
	}

	public void setQueryMap(HashMap<String, SQLQuery> queryMap) {
		this.queryMap = queryMap;
	}
	
	public SQLQuery getQuery(String queryID) {
		return queryMap.get(queryID);
	}
	
	public void merge(SQLQueryMap otherMap) {
		for(String key: otherMap.queryMap.keySet()) {
			queryMap.put(key, otherMap.queryMap.get(key));
		}
	}
}
