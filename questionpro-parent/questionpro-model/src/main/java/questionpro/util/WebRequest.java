package questionpro.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public class WebRequest {
	
	@JsonProperty("header")
	private WebHeader header;
	
	@JsonProperty("body")
	private Object body;

	/**
	 * @return the header
	 */
	public WebHeader getHeader() {
		return header;
	}

	/**
	 * @param header the header to set
	 */
	public void setHeader(WebHeader header) {
		this.header = header;
	}

	/**
	 * @return the body
	 */
	public Object getBody() {
		return body;
	}

	/**
	 * @param body the body to set
	 */
	public void setBody(Object body) {
		this.body = body;
	}
}
