package questionpro.util;

public class RequestFormattedIncorrectlyException extends RuntimeException{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3031765971845960732L;

	public RequestFormattedIncorrectlyException(String message) {
		super(message);
	}
	
	public RequestFormattedIncorrectlyException(String message,Throwable t) {
		super(message, t);
	}
}