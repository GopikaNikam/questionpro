package questionpro.util;

import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * A QueryObject wraps a data object and sets values
 * to the statement when it's setValues() is called.
 * Usually this is used in DAO classes and passed from
 * a DAO method to a execute() method of a query operation. 
 * 
 * @author praveenp
 *
 */
public interface SQLQueryObject {

	void setValues(PreparedStatement statement) throws SQLException;
}