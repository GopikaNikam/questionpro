package questionpro.util;

public enum DatabaseEnum {
	/* Oracle database */
	ORACLE,
	
	/* MySQL database */
	MYSQL;
}
