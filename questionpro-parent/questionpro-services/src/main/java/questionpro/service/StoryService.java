package questionpro.service;

import questionpro.util.WebRequest;
import questionpro.util.WebResponse;

public interface StoryService {
 
	public WebResponse createStory(WebRequest webRequest);
	
	public WebResponse fetchStories();

	public WebResponse fetchAllStories();
	
	public WebResponse fetchCommentStories();

}
