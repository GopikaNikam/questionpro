package questionpro.jdbc.dao;

import questionpro.dao.DAOManager;
import questionpro.dao.StoryDAO;

public class JDBCDAOManager implements DAOManager {

	private JDBCStoryDAO storyDAO;
	private javax.sql.DataSource dataSource;
	
	@Override
	public void init() {
		storyDAO.setDaoManager(this);
	}

	@Override
	public void destroy() {}
	
	public javax.sql.DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(javax.sql.DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public StoryDAO getStoryDAO() {
		return storyDAO;
	}

	public void setStoryDAO(StoryDAO storyDAO) {
		this.storyDAO = (JDBCStoryDAO)storyDAO;
	}
}
