package questionpro.jdbc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import questionpro.dao.StoryDAO;
import questionpro.model.Story;
import questionpro.util.AbstractJDBCDAO;

public class JDBCStoryDAO  extends AbstractJDBCDAO implements StoryDAO {
	
	private final static Logger logger = LoggerFactory.getLogger(JDBCStoryDAO.class);
	
	private final static String CREATE_STORY_QUERY = "createStoryQuery";
	private final static String CREATE_KIDS_QUERY = "createKidsQuery";
	private final static String CREATE_PARTS_QUERY = "createPartsQuery";
	private final static String FETCH_STORY_QUERY = "fetchStoryQuery";
	private final static String FETCH_ALL_STORY_QUERY = "fetchAllStoryQuery";
	private final static String FETCH_COMMENT_STORY_QUERY = "fetchCommentStoryQuery";

	private JDBCDAOManager daoManager;

	/**
	 * @return the daoManager
	 */
	public JDBCDAOManager getDaoManager() {
		return daoManager;
	}

	/**
	 * @param daoManager the daoManager to set
	 */
	public void setDaoManager(JDBCDAOManager daoManager) {
		this.daoManager = daoManager;
	}
	
	/* (non-Javadoc)
	 * @see com.ctpl.finvu.common.dataaccess.AbstractJDBCDAO#getDataSource()
	 */
	@Override
	protected DataSource getDataSource() {
		return daoManager.getDataSource();
	}

	@Override
	public boolean saveStory(Story story) {
		PreparedStatement stmt = null;
		String currentQuery = CREATE_STORY_QUERY;
		Connection con = null;
		try {
			con = getDataSource().getConnection();
			stmt = getStatement(currentQuery, (PreparedStatement statement) -> {
				statement.setInt(1, story.getId());
				statement.setString(2, story.getType());
				statement.setString(3, story.getBy());
				statement.setString(4, story.getText());
				statement.setTimestamp(5, new Timestamp(new Date().getTime()));
				statement.setString(6, story.getParent());
				statement.setInt(7, story.getPoll());
				statement.setString(8, story.getUrl());
				statement.setInt(9, story.getScore());
				statement.setString(10, story.getTitle());
				statement.setInt(11, story.getDescendants());
			}, con);
			stmt.executeUpdate();
			stmt.close();
					
			List<Integer> kids = story.getKids();
			currentQuery = CREATE_KIDS_QUERY;
			
			for(int kid : kids) {
				stmt = getStatement(currentQuery, (PreparedStatement statement) -> {
					statement.setInt(1, story.getId());
					statement.setInt(2, kid);
				}, con);
				
				stmt.executeUpdate();
				stmt.close();
			}
			
			List<Integer> parts = story.getParts();
			currentQuery = CREATE_PARTS_QUERY;
			
			for(int part : parts) {
				stmt = getStatement(currentQuery, (PreparedStatement statement) -> {
					statement.setInt(1, story.getId());
					statement.setInt(2, part);
				}, con);
				
				stmt.executeUpdate();
				stmt.close();
			}
			
		} catch(SQLException sqle) {
			logger.error("Error executing query: {}", currentQuery, sqle);
			return true;
		} finally {
			close(con, stmt);
		}
		return false;
	}

	@Override
	public List<Story> fetchStories() {
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = getStatement(FETCH_STORY_QUERY, null);
			rs = stmt.executeQuery();
			List<Story> list = new ArrayList<>();
			
			while(rs.next()) {
				Story story = new Story();
				story.setId(rs.getInt(1));
				story.setType(rs.getString(2));
				story.setBy(rs.getString(3));
				story.setText(rs.getString(4));
				story.setTime(rs.getTimestamp(5));
				story.setParent(rs.getString(6));
				story.setPoll(rs.getInt(7));
				story.setUrl(rs.getString(8));
				story.setScore(rs.getInt(9));
				story.setTitle(rs.getString(10));
				story.setDescendants(rs.getInt(11));
				list.add(story);
			}
			return list;
			
		} catch(SQLException sqle) {
			logger.error("Error executing query: {}", FETCH_STORY_QUERY, sqle);
		} finally {
			close(rs, stmt);
		}	
		return null;
	}
	
	@Override
	public List<Story> fetchAllStories() {
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = getStatement(FETCH_ALL_STORY_QUERY, null);
			rs = stmt.executeQuery();
			List<Story> list = new ArrayList<>();
			
			while(rs.next()) {
				Story story = new Story();
				story.setId(rs.getInt(1));
				story.setType(rs.getString(2));
				story.setBy(rs.getString(3));
				story.setText(rs.getString(4));
				story.setTime(rs.getTimestamp(5));
				story.setParent(rs.getString(6));
				story.setPoll(rs.getInt(7));
				story.setUrl(rs.getString(8));
				story.setScore(rs.getInt(9));
				story.setTitle(rs.getString(10));
				story.setDescendants(rs.getInt(11));
				list.add(story);
			}
			return list;
			
		} catch(SQLException sqle) {
			logger.error("Error executing query: {}", FETCH_ALL_STORY_QUERY, sqle);
		} finally {
			close(rs, stmt);
		}	
		return null;
	}

	@Override
	public List<Story> fetchCommentStories() {
		ResultSet rs = null;
		PreparedStatement stmt = null;
		
		try {
			stmt = getStatement(FETCH_COMMENT_STORY_QUERY, null);
			rs = stmt.executeQuery();
			List<Story> list = new ArrayList<>();
			
			while(rs.next()) {
				Story story = new Story();
				story.setId(rs.getInt(1));
				story.setType(rs.getString(2));
				story.setBy(rs.getString(3));
				story.setText(rs.getString(4));
				story.setTime(rs.getTimestamp(5));
				story.setParent(rs.getString(6));
				story.setPoll(rs.getInt(7));
				story.setUrl(rs.getString(8));
				story.setScore(rs.getInt(9));
				story.setTitle(rs.getString(10));
				story.setDescendants(rs.getInt(11));
				list.add(story);
			}
			return list;
			
		} catch(SQLException sqle) {
			logger.error("Error executing query: {}", FETCH_COMMENT_STORY_QUERY, sqle);
		} finally {
			close(rs, stmt);
		}	
		return null;
	}
}
